<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']		= 'main/sections/home.min';
		$this->context['pageViewCSS']		= 'main/sections/home';
		$this->context['class_section'] = 'home';
		$this->context['meta'] 					= $this->meta_config[Request::path()];

		return view('main.home.home', $this->context);
	}
}
