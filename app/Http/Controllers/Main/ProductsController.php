<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;

class ProductsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/sections/products.min';
		$this->context['pageViewCSS']	= 'main/sections/products';
		$this->context['meta']				= $this->meta_config[Request::path()];

		return view('main.products.home', $this->context);
	}
}
