<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Session;
use Request;
use Config;

class MainController extends Controller {

	public function __construct() {

		$this->context['section'] = "";
		$this->meta_config = Config::get('meta_config');

		$this->context = array(
			'pageViewCSS' 	=> "",
			'pageViewJS'		=> "",
			'site_url'			=> url('',[]),
			'class_section'	=> '',
			'pagetype' 			=> 'website',
			'image' 				=> url('/images/arm-and-hammer-logo.png'),
			'meta'					=> [
				"title"				=> "Arm & Hammer - The Standard of Purity",
				"desc"				=> "",
				"keyword"			=> "",
				"keyphrases"		=> "",
			],
			"fbid"				=> env("FACEBOOK_APP_ID", "")
		);
	}
}
