<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Illuminate\Http\Request;
use App\Models\QuizEntries;
use Illuminate\Http\JsonResponse;
use Response;
use Illuminate\Support\Facades\Storage;
use Validator;
use Excel;
use DB;

class QuizController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(QuizEntries $quizEntries)
	{
		parent::__construct();

		$this->quizEntries = $quizEntries;

		$this->security = [
			['email' => 'oindypoind@gmail.com', 'password' => '87cdea20f2af2fc53e0ed9a9983e574e914d48a4'],
			['email' => 'alicet@therapyagency.com', 'password' => '3cecb97bf947beb9a41ca7d02b1e626f2e20cdb1'],
			['email' => 'james@therapyagency.com', 'password' => '75a8369f420c81903d53f421b3a594519275e669']
		];
	}

	
	public function QuizOct17()
	{	
		$config = config('quiz');

		$this->context['pageViewCSS']	= 'main/sections/quiz';
		$this->context['class_section'] = '';

		$this->context['pagetype'] = 'website';
		$this->context['image'] = url('/images/quiz/quiz-share.png');
		$this->context['meta']['title'] = 'Who Are Ya?';
		$this->context['meta']['desc'] = 'We&rsquo;re giving away free toothpaste in our exceptional quiz with more personality than a lumberjack riding a polar bear. Yes, you read that right... Take the quiz here and discover your exceptional personality! Or just do it for the free toothpaste...';

		//DB::enableQueryLog();
		$count = $this->quizEntries->today()->get()->count();
		//print_r(DB::getQueryLog());
		//dd($count);
		if ($count >= $config['oct17']['threshold']) {
			$this->context['pageViewJS']	= 'main/sections/quiz-locked.min';
			return view('main.quiz.locked', $this->context);
		} else {
			$this->context['pageViewJS']	= 'main/sections/quiz.min';
			return view('main.quiz.home', $this->context);
		}

		
	}

	public function QuizOct17Store(Request $request) {
		$messages = [
		    'email.unique' => 'You have already claimed a free toothpaste.',
		];

		$validator = Validator::make($request->all(), [
	        'name' => 'required',
	        'address' => 'required',
	        'email' => 'required|email|unique:quiz_entries,email',
	        'marketing' => '',
	        'product' => 'required'
	    ], $messages);

	    if ($validator->fails()) {
            return new JsonResponse(['status' => 'failed', 'errors' => $validator->errors()], 200);
        } else {

        	$time = time();
			$quizEntry = new QuizEntries();
			$quizEntry->ref = $time;
			$quizEntry->name = $request->name;
			$quizEntry->address = $request->address;
			$quizEntry->email = $request->email;
			$quizEntry->marketing = $request->marketing;
			$quizEntry->product = $request->product;
			$quizEntry->save();

        	return new JsonResponse(['status' => 'success'], 200);
        }
	}

	public function terms()
	{	
		$filename = '/pdf/AH-Who-Are-Ya-TCs.pdf';
		return redirect($filename);
	}

	public function manager()
	{	
		$config = config('quiz');
		//dd(sha1('james@therapyagency.com'.'P4nd0r4'));
		return view('main.quiz.manager', $this->context);
	}

	public function stats()
	{	
		$config = config('quiz');

		$count = $this->quizEntries->get()->count();

		$this->context['stats'] = ['threshold' => $config['oct17']['threshold'], 'entries' => $count];


		//dd(sha1('james@therapyagency.com'.'P4nd0r4'));
		return view('main.quiz.manager', $this->context);
	}

	public function threshold(Request $request) {
		if ($this->checkSecurity($request)) {
			$config = config('quiz');
	        $config['oct17']['threshold'] = $request->threshold;
	        $encoded = str_replace(["{","}",":"],["[","]","=>"],stripslashes(json_encode($config)));
	        $output = "<?php return " . $encoded . ";";
	        Storage::disk('config')->put('quiz.php', $output);
	        return redirect('quiz/manager')->with('status', 'Threshold updated!')->with('status-type', 'success');
	    } else {
	    	return redirect('quiz/manager')->with('status', 'Security Failed!')->with('status-type', 'danger');
	    }
	}

	public function export(Request $request) {
		if ($this->checkSecurity($request)) {
			if ($request->entriesrange == 'today') {
				$data = $this->quizEntries->today()->get()->toArray();
			} else {
				$data = $this->quizEntries->get()->toArray();
			}
			Excel::create('AHQuizOct17', function($excel) use($data) {
			    $excel->sheet('Entries', function($sheet) use($data) {
			        $sheet->fromArray($data);

			    });
			})->export('xls');
		} else {
			return redirect('quiz/manager')->with('status', 'Security Failed!')->with('status-type', 'danger');
		}
	}

	private function checkSecurity($request) {
		$password = sha1($request->email.$request->password);
		$found = false;
		foreach($this->security as $user) {
			if ($password == $user['password']) {
				$found = true;
			}
		}
		return $found;
	}
}

