<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;

class ScienceController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']		= 'main/sections/science.min';
		$this->context['pageViewCSS']		= 'main/sections/science';
		$this->context['meta']					= $this->meta_config[Request::path()];

		return view('main.science.home', $this->context);
	}
}
