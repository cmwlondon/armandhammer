<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Request;
use Mail;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';

		$this->context['class_section'] = '';

		return view('main.misc.home', $this->context);
	}

	public function heritage()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/heritage';

		$this->context['class_section'] = '';

		return view('main.misc.heritage', $this->context);
	}

	public function oral()
	{	
		$this->context['pageViewJS']	= 'main/sections/oral.min';
		$this->context['pageViewCSS']	= 'main/sections/oral';

		$this->context['class_section'] = '';

		return view('main.misc.oral', $this->context);
	}

	public function prizedraw()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/misc';

		$this->context['class_section'] = '';

		return view('main.misc.prizedraw', $this->context);
	}

	public function cookies()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/misc';

		$this->context['class_section'] = '';

		return view('main.misc.cookies', $this->context);
	}

	public function privacy()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/misc';

		$this->context['class_section'] = '';

		return view('main.misc.privacy', $this->context);
	}

	public function thirdparty()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/misc';

		$this->context['class_section'] = '';

		return view('main.misc.third-party', $this->context);
	}

	public function mailtest()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/misc';

		$this->context['class_section'] = '';

		$this->context['noreply'] = env('MAIL_NOREPLY');
		$this->context['systo'] = env('MAIL_SYS_TO');
		$this->context['adminto'] = env('MAIL_ADMIN_TO');
		$this->context['admincc'] = env('MAIL_ADMIN_CC');
		$this->context['adminbcc'] = env('MAIL_ADMIN_BCC');

		$details = [
			'title'			=> 'Mr',
			'firstname'		=> 'Ben',
			'lastname'		=> 'Stephens',
			'company'		=> 'Stack',
			'phone'			=> '353534535',
			'email'			=> 'ben.stephsn@stackworks.com',
			'product'		=> 'advertizotron 9000',
			'comments'		=> 'mega ROI',
			'mfgcode'		=> 'armandhammer'
		];

		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
			$message->from('armandhammer@armandhammer.co.uk', 'arm and hammer');
			$message->cc(env('MAIL_SYS_TO'));
			$message->to(env('MAIL_ADMIN_TO'));
		    $message->subject('arm and hammer Enquiry');
		});

		return view('main.misc.mailtest', $this->context);
	}

	// AprilFools
	
	public function AprilFools()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/coming-soon';

		$this->context['class_section'] = '';

		$this->context['reveal'] = false;
		$revealTime = 1491004800 - 3600;
		$forced = Request::input('forced');
		if (time() > $revealTime || $forced == "true") {
			if (Request::segment(1) != 'aprilfools') {
				return redirect('aprilfools');
			}
			$this->context['reveal'] = true;
		}

		return view('main.misc.aprilfools', $this->context);
	}
}

