<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use App\Http\Requests\Main\SupportRequest;
use App\Models\Support;
use Mail;

class SupportController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/sections/support';

		$this->context['class_section'] = '';

		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];

		return view('main.support.home', $this->context);
	}



	public function store(SupportRequest $request, Support $support)
	{
		// Prepare the email to send.
		$details = array(
			'title'			=> $request->title,
			'firstname'		=> $request->firstname,
			'lastname'		=> $request->lastname,
			'company'		=> $request->company,
			'address'		=> '-',//$request->address,
			'city'			=> '-',//$request->city,
			'county'		=> '-',//$request->country,
			'postcode'			=> '-',//$request->postcode,
			'country'		=> '-',//$request->country,
			'phone'			=> $request->phone,
			'email'			=> $request->email,
			'product'		=> ($request->product !== '' && gettype($request->product) !== 'NULL') ? $request->product : '-',
			'comments'		=> $request->comments
		);

		$support->fill( $details );
		$support->save();

		$this->sendSYsMail($details);

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for contacting Arm &amp; Hammer.');

		return redirect('contact-us')->with('response', $this->response);
	}


	private function sendMail($details)
	{
		Mail::send('emails.enquiry', $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_ADMIN_TO'));

		    if (!is_null(env('MAIL_ADMIN_CC')) && env('MAIL_ADMIN_CC') != "") {
		    	$message->cc(env('MAIL_ADMIN_CC'));
		    }
		    if (!is_null(env('MAIL_ADMIN_BCC')) && env('MAIL_ADMIN_BCC') != "") {
		    	$message->bcc(env('MAIL_ADMIN_BCC'));
		    }
		    $message->subject('UK ChurchDwight Enquiry');
		});
	}

	private function sendSYsMail($details)
	{
		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_SYS_TO'));
		    $message->subject('UK ChurchDwight Enquiry');
		});
	}



}
