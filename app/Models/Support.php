<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supports';


	protected $fillable = ['title','firstname','lastname','company','address','city','county','postcode','country','phone','email','product','comments'];

	/* fileds displayed in contact us / support form
	'title','firstname','lastname','company','phone','email','product','comments'
	*/

}
