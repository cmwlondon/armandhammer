<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class QuizEntries extends Model
{
    
  protected $fillable = [
    'ref','name','address','email','marketing','product'
  ];

  /**
   * Scope to get only fields which we would want to make public.
   */
  public function scopePublicFields($query)
  {
    return $query->select('ref');
  }

  /**
   * Scope to get only entries from today.
   */
  public function scopeToday($query)
  {
    //$dt = Carbon::create(2017, 10, 26, 8, 0, 1);
    $dt = Carbon::now();

    $dtActivate = Carbon::create(2017, 10, 26, 8, 0, 0);
    if ($dt->gt($dtActivate)) {
      //$dt->addHour();
      $dt->subHours(9);

      $dtStart = $dt->startOfDay()->addHours(8)->toDateTimeString();     
      $dtEnd = $dt->addHours(24)->toDateTimeString();     

      //echo "Start of quiz day is:" . $dtStart . '<br/>';
      //echo "end of quiz day is:" . $dtEnd;
      //die();
      return $query->whereRaw("`created_at` >= '".$dtStart."' AND `created_at` < '".$dtEnd."'");
    } else {
      return $query->whereRaw("DATE(`created_at`) < CURRENT_DATE");
    }    
  }
}
