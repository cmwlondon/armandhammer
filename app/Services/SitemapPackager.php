<?php namespace App\Services;

use Response;
use Carbon\Carbon;
use Storage;
use Illuminate\Routing\RouteCollection;
use LSS\Array2XML;

class SitemapPackager {

	public function __construct(RouteCollection $routeCollection)
	{
		Carbon::setLocale('en');

		$this->routeCollection = $routeCollection;

		$this->sitemap = [
			'@attributes' => [
				'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
				'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'
			],

			'url' => []
		];


	}

	public function create() {
		$now = new Carbon();
		$now = $now->toW3cString();

		$this->addUrl(url('/'),$now,'daily','1.0');
		$this->addUrl(url('/products'),$now,'monthly','0.9');
		$this->addUrl(url('/products/the-total-taste'),$now,'monthly','0.9');
		$this->addUrl(url('/products/the-white-answers'),$now,'monthly','0.9');
		$this->addUrl(url('/products/the-sensitive-types'),$now,'monthly','0.9');
		$this->addUrl(url('/products/the-enamel-pro'),$now,'monthly','0.9');
		$this->addUrl(url('/products/the-cavity-carer'),$now,'monthly','0.9');
		// $this->addUrl(url('/quiz'),$now,'monthly','0.9');
		$this->addUrl(url('/contact-us'),$now,'monthly','0.9');
		$this->addUrl(url('/our-heritage'),$now,'monthly','0.9');
		$this->addUrl(url('/where-to-buy'),$now,'monthly','0.9');
		$this->addUrl(url('/oral-health'),$now,'monthly','0.9');
		$this->addUrl(url('/science'),$now,'monthly','0.9');
		$this->addUrl(url('/terms-&-conditions'),$now,'monthly','0.9');
		$this->addUrl(url('/cookie-notice'),$now,'monthly','0.9');
		$this->addUrl(url('/privacy-policy'),$now,'monthly','0.9');

		// Convert and save
		$xml = Array2XML::createXML('urlset', $this->sitemap);
		// above line generates error:
		// DOMDocument::createTextNode() expects parameter 1 to be string, object given

		$response = $xml->saveXML();
		//dd(Storage::disk('public'));
		Storage::disk('public')->put('sitemap.xml', $response);

		// $response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		
		return Response::make($response, '200')->header('Content-Type', 'text/xml');
	}

	private function addUrl($url,$lastmod,$freq,$priority) {
		$this->sitemap['url'][] = ['loc' => $url,
							'lastmod' => $lastmod,
							'changefreq' => $freq,
							'priority' => $priority
							];
	}
}
