<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Main\HomeController@index');
Route::get('/products', 'Main\ProductsController@index');
Route::get('/products/{family}', 'Main\ProductsController@index');

Route::get('/oral-health', 		'Main\MiscController@oral');
Route::get('/science', 			'Main\ScienceController@index');
Route::get('/where-to-buy', 	'Main\BuyController@index');

// Route::get('/contact-us', 'Main\SupportController@index');
// Route::post('/contact-us', 'Main\SupportController@store');

Route::get('/our-heritage', 'Main\MiscController@heritage');
Route::get('/cookie-notice', 'Main\MiscController@cookies');
Route::get('/privacy-policy', 'Main\MiscController@privacy');
Route::get('/third-party-information-collection', 'Main\MiscController@thirdparty');

// Route::get('/mailtest', 'Main\MiscController@mailtest');

Route::get('/reviewprizedraw', 'Main\MiscController@prizedraw');

Route::get('/AHLHT', 'Main\MiscController@AprilFools');
Route::get('/aprilfools', 'Main\MiscController@AprilFools');

/*
Route::get('/quiz', 'Main\QuizController@QuizOct17');
Route::post('/quiz/store', 'Main\QuizController@QuizOct17Store');
Route::get('/quiz/manager', 'Main\QuizController@manager');
Route::post('/quiz/manager/export', 'Main\QuizController@export');
Route::post('/quiz/manager/threshold', 'Main\QuizController@threshold');
Route::post('/quiz/manager/stats', 'Main\QuizController@stats');
Route::get('/quiz/terms', 'Main\QuizController@terms');
*/

Route::get('/sitemap', 'Main\SitemapController@refresh');

Route::get('/gdpr', function () {
    $exitCode = Artisan::call('gdpr:check', []);
    echo $exitCode;
});
