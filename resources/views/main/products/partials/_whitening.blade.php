<div class="inner animatedParent animateOnce" data-ref="white-answer">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range/white-answer.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 xlg-up-before-3 mt4 tac">
			<a href="#advance-white-extreme" class="bProductsRoutedTop"><img src="/images/products/pack-advance-white.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>

	<div class="row full productBlock answersBody tac">
		<div class="columns span-10 before-1">
			<a name="#advance-white-extreme"></a>
			<h3>Advance White Pro<sup>&trade;</sup> Toothpaste</h3>
			<div class="bar"></div>
			<h4>Up to 3 shades whiter teeth in 6 weeks</h4>
			<p>Give your nashers a whitening boost. Banish stains with a little added buffing action to leave your teeth brighter and whiter than ever before. *TING*… Hear that? That&rsquo;s your whiter, shinier teeth.</p>
		</div>
	</div>

	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')
</div>
