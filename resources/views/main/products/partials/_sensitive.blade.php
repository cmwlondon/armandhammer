<div class="inner animatedParent animateOnce" data-ref="sensitive-type">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range/sensitive-type.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 xlg-up-before-3 mt4 tac">
			<a href="#sensitive-pro-repair" class="bProductsRoutedTop"><img src="/images/products/pack-sensitive-pro-repair.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>

	<div class="row full productBlock sensitiveBody tac">
		<div class="columns span-10 before-1">
			<a name="#sensitive-pro-repair"></a>
			<h3>Sensitive Pro Repair Toothpaste</h3>
			<div class="bar"></div>
			<h4>FOR UP TO 16 WEEKS PAIN RELIEF</h4>
			<p>Ice cream loving, Frappuccino drinking, ice munching fiend? Well, don&rsquo;t let silly sensitive teeth stop you from enjoying the cooler things in life. This is our sensitive toothpaste that fixes the cause of sensitivity by sealing teeth and protecting exposed nerves with patented Liquid Calcium&trade; technology, to provide lasting pain relief.* While the baking soda cleans and whitens more gently.</p>
			<p>*With regular use for 8 weeks. Relief then continues for another 8 weeks.</p>

		</div>
	</div>

	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')
</div>
