<div class="row full productBlock totalBody tac directions">
  <div class="columns span-10 before-1">
    <div class="bar"></div>
    <h4>Directions:</h4>
    <p><b>Children under 7:</b> Use a pea sized amount and supervise brushing to minimize swallowing.  In case of intake of fluoride from other sources, consult a dentist or doctor</p>
    <p><b>Adults & Children 2 years and older:</b>  Brush teeth thoroughly at least twice a day, or use as directed by a dentist or doctor</p>
  </div>
</div>
