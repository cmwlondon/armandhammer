<div class="inner animatedParent animateOnce" data-ref="the-cavity-carer">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range-the-cavity-carer.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 xlg-up-before-3 mt4 tac">
			<a href="#advance-cavity-care" class="bProductsRoutedTop"><img src="/images/products/pack-advance-cavity-care.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>


	{{-- <div class="row full productBlock cavityBlock tac">
		<div class="columns span-10 before-1">
			<p>Sugar, it tastes so good but it hurts teeth so bad. Our nifty cavity caring formula is armed with a sugar defense… think of it like a whole bunch of little soldiers stood around your teeth fighting off all the sugary nasties that can lead to cavities. Yep, it&rsquo;s like that.</p>
		</div>
	</div> --}}

	<div class="row full productBlock cavityBody tac">
		<div class="columns span-10 before-1">
			<a name="#advance-cavity-care"></a>
			<h3>Advance Cavity Care Toothpaste</h3>
			<div class="bar"></div>
			<h4>HELPS TO NEUTRALISE PLAQUE ACID</h4>
			<p>Sugar, it tastes so good but it hurts teeth so bad and causes a whole load of issues! Our nifty cavity caring formula is armed with SUGAR DEFENSE  technology that works by neautralising plaque acids… think of it like a whole bunch of little soldiers stood around your teeth helping to care for your mouth. Yep, it&rsquo;s like that.</p>
		</div>
	</div>
	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')
</div>
