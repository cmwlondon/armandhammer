<div class="inner animatedParent animateOnce" data-ref="enamel-pro">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range/enamel-pro.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 xlg-up-before-3 mt4 tac">
			<a href="#enamel-pro-repair" class="bProductsRoutedTop"><img src="/images/products/pack-enamel-pro-repair.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>


	{{-- <div class="row full productBlock enamelBlock tac">
		<div class="columns span-10 before-1">
			<p>Acid erosion, sounds like something from geography class but it actually happens to your teeth. Our exceptional deep cleaning enamel formula takes on acid erosion by filling in tooth crevices and also helps to restore surface enamel for healthier, stronger teeth, a bit like the Hulk, but not green, or scary looking, so not really like the Hulk at all. </p>
		</div>
	</div> --}}

	<div class="row full productBlock enamelBody tac">
		<div class="columns span-10 before-1">
			<a name="#enamel-pro-repair"></a>
			<h3>Enamel Pro Repair Toothpaste</h3>
			<div class="bar"></div>
			<h4>Helps protect enamel surface from acid erosion</h4>
			<p>Our exceptional baking soda and Liquid Calcium&trade; formula takes on acid erosion by filling in tooth crevices and also helps to restore enamel surface for healthier, stronger teeth, a bit like the Hulk, but not green, or scary looking, so not really like the Hulk at all.</p>
		</div>
	</div>
	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')
</div>
