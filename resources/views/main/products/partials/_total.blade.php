<div class="inner animatedParent animateOnce" data-ref="total-protection">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range/total-protection.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 xlg-up-before-3 mt4 tac">
			<a href="#total-pro-clean-and-repair" class="bProductsRoutedTop"><img src="/images/products/pack-whitening-pro-protect.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>

	<div class="row full productBlock totalBody tac">
		<div class="columns span-10 before-1">
			<a name="#whitening-pro-protect"></a>
			<h3>Whitening Pro Protect<sup>&trade;</sup> Toothpaste</h3>
			<div class="bar"></div>
			<h4>For whiter healthier teeth</h4>
			<p>Introducing our latest whitening formula, NEW Arm & Hammer Whitening Pro Protect<sup>&trade;</sup> combines the natural power of baking soda with our unique Liquid Calcium<sup>&trade;</sup> technology for stronger, whiter teeth. The 5-action system deep cleans, polishes, whitens, protects and restores for an all-round healthier smile.</p>
		</div>
	</div>

	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')

</div>
