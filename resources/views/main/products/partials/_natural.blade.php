<div class="inner animatedParent animateOnce" data-ref="natures-finest">
	<div class="row home">
		<div class="columns span-12 tac">
			<img src="/images/products/range/natures-finest.png" class="m100"/>
		</div>
		<div class="columns span-12 xlg-up-6 mt4 tac">
			<a href="#natural-clean+white" class="bProductsRoutedTop"><img src="/images/products/pack-natural-clean+white.png" class="range pack animated growIn" data-label="range" data-id="1"/></a>
		</div>

		<div class="columns span-12 xlg-up-6 mt4 tac">
			<a href="#charcoal-white" class="bProductsRoutedTop"><img src="/images/products/pack-charcoal-white.png" class="range pack animated growIn" data-label="range" data-id="2"/></a>
		</div>

		<div class="columns span-12 tac">
			<a href="/products" class="bProductsRouted blkButton mb2 vtr">View The Range</a>
		</div>
	</div>


	{{-- <div class="row full productBlock answersBlock tac">
		<div class="columns span-10 before-1">
			<p>Give your nashers a whiteness boost. Our immense baking soda formula deep cleanses teeth, ridding them of plaque and banishing stains with a little added buffing action to leave your teeth brighter and whiter then ever before. *TING*… Hear that? That&rsquo;s your shiny, white teeth. </p>
		</div>
	</div> --}}

	<div class="row full productBlock naturalBlock tac">
		<div class="columns span-10 before-1">
			<a name="#natural-clean+white"></a>
			<h3>Natural CLEAN+WHITE<sup>&trade;</sup> Toothpaste</h3>
			<div class="bar"></div>
			<h4>For a whiter smile in 5 days</h4>
			<p>Looking for a more natural way to whiten your teeth? Look no further than Arm &amp; Hammer Natural Clean+White<sup>&trade;</sup>. Our 95% ingredients from natural origin formula is powered by our miracle ingredient, baking soda, working its way around your mouth to help remove stains, reaching areas that normal brushing can&rsquo;t. It&rsquo;s completely preservative free, vegan friendly and still contains all important fluoride for cavity protection.</p>
		</div>
	</div>
	<div class="row full productBlock naturalBlock tac">
		<div class="columns span-10 before-1">
			<a name="#charcoal-white"></a>
			<h3>Charcoal White<sup>&trade;</sup> Toothpaste</h3>
			<div class="bar"></div>
			<h4>Removes 100% more surface stains</h4>
			<p>We decided that the exceptional deep cleaning and whitening power of baking soda wasn&rsquo;t quite enough for some people so we added another natural ingredient, activated charcoal! Introducing NEW Arm & Hammer Charcoal White<sup>&trade;</sup>. The powerful combination removes up to 100% more surface stains than a regular toothpaste for a clinically proven whiter smile in just 3 days. </p>
		</div>
	</div>
	@include('main.products.partials._wtb')
	@include('main.products.partials._directions')
</div>
