@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row home">
		<div class="columns span-12 tac headFixed">
			<h1>Meet the Vegan Family</h1>
			<div class="controls hidden">
				<div class="arrow left"><i class="carousel-icon icon-arrow-left"></i></div>
				<div class="arrow right"><i class="carousel-icon icon-arrow-right"></i></div>
			</div>
		</div>
	</div>

	<div class="sliding-block">
		@include('main.products.partials._range')
		@include('main.products.partials._sensitive')
		@include('main.products.partials._whitening')
		@include('main.products.partials._enamel')
		@include('main.products.partials._total')
		{{-- @include('main.products.partials._cavity') --}}
		@include('main.products.partials._natural')
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
