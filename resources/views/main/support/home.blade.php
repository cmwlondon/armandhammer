@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="row mb3">
		<div class="columns span-12 intro">
			<h1>CONTACT US</h1>
			<p>If you are a consumer and wish to contact us, please call 0800 121 6080 or complete the enquiry form below.</p>
		</div>
		<div class="columns span-9 sm-12 md-12 panel">
			@include('main.layouts.partials._shadow')

			<div class="form">
				@if (session('response'))
					<div class="formRow">
						<h3>{!! session('response.message') !!}</h3>
					</div>
				@else


				{!! Form::open(array('url' => ['contact-us'], 'method' => 'POST', 'id' => 'supportForm') )!!}

				<div class="formRow">
					{!! Form::label('title', 'Title: *') !!}
					{!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
					{!! $errors->first('title', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('firstname', 'First Name: *') !!}
					{!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
					{!! $errors->first('firstname', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('lastname', 'Last Name: *') !!}
					{!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
					{!! $errors->first('lastname', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('company', 'Company:') !!}
					{!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'input']) !!}
					{!! $errors->first('company', '<small class="error">:message</small>') !!}
				</div>

				{{-- 
				<div class="formRow">
					{!! Form::label('address', 'Address:') !!}
					{!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
					{!! $errors->first('address', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('city', 'City:') !!}
					{!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
					{!! $errors->first('city', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('county', 'County:') !!}
					{!! Form::text('county',null,['placeholder' => '', 'id' => 'county', 'class' => 'input']) !!}
					{!! $errors->first('county', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('postcode', 'Postcode:') !!}
					{!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
					{!! $errors->first('postcode', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('country', 'Country:') !!}
					{!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
					{!! $errors->first('country', '<small class="error">:message</small>') !!}
				</div>
				--}}

				<div class="formRow">
					{!! Form::label('phone', 'Phone Number: *') !!}
					{!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
					{!! $errors->first('phone', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('email', 'Email Address: *') !!}
					{!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
					{!! $errors->first('email', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('product', 'Product:') !!}
					{!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
					{!! $errors->first('product', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('comments', 'Comments: *') !!}
					{!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'input']) !!}
					{!! $errors->first('comments', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow tar">
					{!! Form::submit('Submit', array('class' => 'btn-submit', 'id' => 'bSubmit')) !!}
				</div>

				{!! Form::close() !!}

				@endif
			</div>

		</div>
		<div class="columns span-3 sm-12 md-12 panel">
			@include('main.layouts.partials._shadow')
			<h5>For Media Enquiries Contact:</h5>
			<p>JWPR<br/>3 CAWNPORE STREET<br/>LONDON<br/>SE19 1PF<br/><a href="mailto:geri@jwpr.co.uk">GERI@JWPR.CO.UK</a><br/>TEL: 020 8670 5337</p>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
