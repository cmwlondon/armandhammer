<!doctype html>
<html class="no-js" lang="en">
	<head>
		<!-- OneTrust Cookies Consent Notice start for armandhammer.co.uk -->
		<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="802fb0ab-2385-4cc8-b8de-7414a329c2e0" ></script>
		<script type="text/javascript">
		function OptanonWrapper() { }
		</script>
		<!-- OneTrust Cookies Consent Notice end for armandhammer.co.uk -->
		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

		@include('main.layouts.partials._meta')

		@include('main.layouts.partials._favicon')

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/css/main/styles.min.css?t={{ time() }}" toRefresh="/css/main/styles.css?t={{ time() }}"/>
		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css?t={{ time() }}" toRefresh="/css/{{{$pageViewCSS}}}.css?t={{ time() }}"/>
		@endif

		<!--[if lt IE 9]>
			<link rel="stylesheet" href="/css/main/ie8.css?v=1.0">
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
		    .gradient {
		       filter: none!important;
		    }
		  </style>
		<![endif]-->

		<!-- Facebook Pixel Code -->
		<!-- opentrust targeting cookie 'optanon-category-4' -->
		<script type="text/plain" class="optanon-category-4">
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		 fbq('init', '637957292974237');
		fbq('track', 'PageView');
		</script>
		<noscript>
		 <img height="1" width="1"
		src="https://www.facebook.com/tr?id=637957292974237&ev=PageView
		&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->
	</head>

	<body>
		<a name="top"></a>
		
		{{-- 
		<div class="cookie-disclaimer">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
		</svg></div>
		--}}

		<div id="wrapper">
			@include('main.layouts.partials._mobile-menu')

			<div id="mobile-white-out" class="hidden"></div>
			<div id="headerWrapper" class="{{{ $class_section }}}">
				@include('main.layouts.partials._main-menu')

				@yield('header')
			</div>

			<div id="contentWrapper" class="{{{ $class_section }}}">
				<a name="content-section"></a>
				@yield('content')
			</div>
			<div id="footerWrapper">
				@yield('footer')
			</div>
		</div>

		<div id="noIE">
			<div class="tbl">
				<div class="inner">Sorry your browser is not supported, please use either IE9 or later, Chrome, Firfox or Safari.</div>
			</div>
		</div>

		<script>
			var site_url = "{{ $site_url }}";
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js?1']) !!}

		@if (App::environment() == 'production')
		<!-- Google Analytics -->
		<script type="text/plain" class="optanon-category-2">
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-5129635-1', 'auto');
		  ga('send', 'pageview');

		</script>
		@endif
	</body>
</html>
