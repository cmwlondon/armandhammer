<div id="bubbles-canvas"></div>
<div class="menu-desktop">
	<ul class="menu-ul">
		<li><a class="menu-link-two" href="/oral-health" data-id="1">Oral Health</a></li>

		<li class="dropdown"><a class="menu-link-three" href="/science" data-id="2">Our Science</a><ul class="sub_menu">
				<li><a class="menu-link-sub-six bScienceRouted" href="/science#baking-soda">Baking Soda</a></li>
				<li><a class="menu-link-sub-seven bScienceRouted" href="/science#active-calcium">Active Calcium</a></li>
			</ul></li>

		<li class="logo">&nbsp;<a href="/"><img src="/images/logo.svg"/></a></li>

		<li class="dropdown"><a class="menu-link-one bProductsRouted" href="/products" data-id="3">Our Products</a>
			<ul class="sub_menu">
				<li class="total"><a class="menu-link-sub-one bProductsRouted" href="/products/total-protection">The Total Protection</a></li>
				<li class="answers"><a class="menu-link-sub-two bProductsRouted" href="/products/white-answer">The White Answer</a></li>
				<li class="sensitive"><a class="menu-link-sub-three bProductsRouted" href="/products/sensitive-type">The Sensitive Type</a></li>
				<li class="enamel"><a class="menu-link-sub-four bProductsRouted" href="/products/enamel-pro">The Enamel Pro</a></li>
				<li class="natural"><a class="menu-link-sub-five bProductsRouted" href="/products/natures-finest">Nature's Finest</a></li>
			</ul>
		</li>



		<li><a class="menu-link-four" href="/where-to-buy" data-id="4">Where to buy</a></li>
	</ul>
</div>

<div class="menu-mobile">
	<a href="/"><img src="/images/logo.svg" class="logo"/></a>

	<div class="menu-opener"><i class="icon-menu"></i></div>
</div>
