<div class="row full rel">
	<div class="columns span-12 footer-menu">
		<ul>
			{{-- <li><a href="/contact-us">Contact Us</a></li> --}}
			<li><a href="/our-heritage">Our Heritage</a></li>
			<li><a href="http://www.churchdwight.co.uk/" target="_blank">Visit Church &amp; Dwight</a></li>
			<li><a href="/cookie-notice">Cookie Notice</a></li>
			<li><a href="/privacy-policy">Privacy Policy</a></li>
		</ul>
		<!-- OneTrust Cookies Settings button start -->
		<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
		<!-- OneTrust Cookies Settings button end -->

		<p class="mb0">&copy; Church &amp; Dwight Co. Inc 2021 <br class="hidden vis-sm vis-md"/>All rights reserved</p>
	</div>
	<div class="social">
		<ul>
			<li>
				<a href="//www.facebook.com/armandhammeruk" target="_blank">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 84 84" enable-background="new 0 0 84 84" xml:space="preserve">
					<g class="social-icon">
						<circle fill="#5EC8D8" cx="42" cy="42" r="42"/>
						<path class="inner" d="M48,84V51.5h10.9l1.6-12.7H48v-8.1c0-3.7,1-6.2,6.3-6.2l6.7,0V13.2c-1.2-0.2-5.2-0.5-9.8-0.5
							c-9.7,0-16.3,5.9-16.3,16.8v9.3h-11v12.7h11V84H48z"/>
					</g>
					</svg>
				</a>
			</li>

			{{-- https://www.instagram.com/armandhammer/?hl=en --}}

			<li>
				<a href="https://www.instagram.com/armandhammer/?hl=en" target="_blank">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 84 84" enable-background="new 0 0 84 84" xml:space="preserve" class="instagram">
					<g class="social-icon">
						<circle fill="#5EC8D8" cx="42" cy="42" r="42"/>
						<path  class="inner instagram" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" />
					</g>
					</svg>
<!--
Font Awesome Free 5.4.1 by @fontawesome - https://fontawesome.com
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
-->
				</a>
			</li>
		</ul>
	</div>

</div>
