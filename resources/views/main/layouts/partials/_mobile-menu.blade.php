<div class="slide-menu">
	<div class="bg"></div>
	<div class="head"><i class="icon-cancel-circle close"></i></div>
	<ul class="mobile-menu-ul">
		<li><a href="/oral-health"><i class="icon-right-open"></i> Oral Health</a></li>

		<li class="dropdown"><a href="/science"><i class="icon-right-open"></i> Our Science</a><ul class="sub_menu">
				<li><a href="/science#baking-soda" class="bScienceRouted">Baking Soda</a></li>
				<li><a href="/science#active-calcium" class="bScienceRouted">Active Calcium</a></li>
			</ul></li>

			{{--
			<li class="dropdown"><a href="/products" class="bProductsRouted"><i class="icon-right-open"></i> Our Products</a><ul class="sub_menu">
				<li><a class="bProductsRouted" href="/products/the-total-taste">Total Care</a></li>
				<li><a class="bProductsRouted" href="/products/the-white-answers">Whitening</a></li>
				<li><a class="bProductsRouted" href="/products/the-sensitive-types">Sensitive</a></li>
				<li><a class="bProductsRouted" href="/products/the-enamel-pro">Enamel Care</a></li>
				<li><a class="bProductsRouted" href="/products/the-cavity-carer">Cavities</a></li>
			</ul></li>
			--}}

			<li class="dropdown"><a href="/products" class="bProductsRouted"><i class="icon-right-open"></i> Our Products</a><ul class="sub_menu">
				<li><a class="bProductsRouted" href="/products/total-protection">The Total Protection</a></li>
				<li><a class="bProductsRouted" href="/products/white-answer">The White Answer</a></li>
				<li><a class="bProductsRouted" href="/products/sensitive-type">The Sensitive Type</a></li>
				<li><a class="bProductsRouted" href="/products/enamel-pro">The Enamel Pro</a></li>
				<li><a class="bProductsRouted" href="/products/natures-finest">Nature's Finest</a></li>
			</ul></li>

		<li><a href="/where-to-buy"><i class="icon-right-open"></i> Where to buy</a></li>
	</ul>
</div>