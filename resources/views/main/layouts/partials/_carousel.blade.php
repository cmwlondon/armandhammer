<div class="carousel" data-group="<?php echo $group; ?>" data-max="<?php echo count($products); ?>">
	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALIAAAAZCAMAAACij4R3AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////AAAAVcLTfgAAAAF0Uk5TAEDm2GYAAAAeSURBVHja7MExAQAAAMKg9U9tCj+gAAAAADiZAAMAEXsAAUdXAc0AAAAASUVORK5CYII=" class="shim"/>
<?php
	/*
	$productLinks = [
		'advance-cavity-care' => 'the-cavity-carer#advance-cavity-care',
		'advance-sensitive-care' => 'the-sensitive-types#advance-sensitive-care',
		'advance-white' => 'the-white-answers#advance-white-extreme',
		'enamel-pro-repair' => 'the-enamel-pro#enamel-pro-repair',
		'sensitive-pro-repair' => 'the-sensitive-types#sensitive-pro-repair',
		'total-pro' => 'the-total-taste#total-pro-clean-and-repair',
		'truly-radiant' => 'the-white-answers#truly-radiant-whitening'
	];
	*/
	$productLinks = [
		'whitening-pro-protect' => 'total-protection#whitening-pro-protect',
		'advance-white' => 'white-answer#advance-white-extreme',
		'sensitive-pro-repair' => 'sensitive-type#sensitive-pro-repair',
		'natural-clean+white' => 'natures-finest#natural-clean+white',
		'charcoal-white' => 'natures-finest#charcoal-white',
		'enamel-pro-repair' => 'enamel-pro#enamel-pro-repair'
	];
	$id = 0;
	foreach ($products as $product) {

		$id = ($id == count($products)-1) ? 0 : $id+1;
?>
	<a href="/products/<?php echo $productLinks[$product]; ?>"><img src="/images/products/pack-<?php echo $product; ?>.png" class="slide" data-id="<?php echo $id; ?>"/></a>
<?php 
	}
?>
</div>

<div class="controls" data-group="<?php echo $group; ?>">
	<div class="arrow left"><i class="carousel-icon icon-arrow-left"></i></div>
	<div class="arrow right"><i class="carousel-icon icon-arrow-right"></i></div>
</div>