<meta charset="utf-8" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />

<title>{{{$meta['title']}}}</title>

@if (App::environment() == 'production')
  <meta name="robots" content="INDEX, FOLLOW" />
@else
  <meta name="robots" content="NOINDEX, NOFOLLOW" />
@endif

<meta name="description" content="{{{$meta['desc']}}}" />
<meta name="keyword" content="{{{$meta['keyword']}}}" />
<meta name="keyphrases" content="{{{$meta['keyphrases']}}}" />

<link rel="canonical" href="{{{Request::url()}}}" />

<meta property="og:locale" content="en_GB" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:type" content="{{{$pagetype}}}" />
<meta property="og:image" content="{{{$image}}}" />
<meta property="og:title" content="{{{$meta['title']}}}" />
<meta property="og:description" content="{{{$meta['desc']}}}" />
<meta property="og:site_name" content="Arm &amp; Hammer" />
<meta property="fb:app_id" content="{{{$fbid}}}"/>
