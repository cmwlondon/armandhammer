@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div style="min-height:300px;">
	@if ($reveal)
	<img src="/images/coming-soon/left-handed-toothbrush.png" class="centered" alt="The Arm & Hammer Left Handed Toothbrush"/>
	@else
	<img src="/images/coming-soon/coming-soon-2017.png" class="centered" alt="Coming Soon - The New Right Way To Brush"/>
	@endif
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
