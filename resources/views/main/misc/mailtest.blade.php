@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 panel legal" style="min-height:450px;">
			<img src="/images/shadow.png" class="shadow">
			<h1>mail test</h1>
			<p>noreply: {{$noreply}}</p>
			<p>systo: {{$systo}}</p>
			<p>adminto: {{$adminto}}</p>
			<p>admincc: {{$admincc}}</p>
			<p>adminbcc: {{$adminbcc}}</p>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
