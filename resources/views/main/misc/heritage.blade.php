@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 intro" >
			<h1>OUR HERITAGE</h1>
		</div>
		<div class="columns span-12 mb2" >
			<div class="block b1 first">
				<div class="vert"><img src="/images/heritage/side1.svg" alt="Ancient Times" title="Ancient Times"/></div>
				<div class="img"><img src="/images/heritage/blk1-image.jpg" class="mt3"/></div>
				<div class="ball"></div>
				<div class="items">
					<div class="item">
						<h3>4 million years ago</h3>
						<p>Trona deposits are formed after the evaporation of great salt lakes in Wyoming USA, as well as in Kenya, Egypt, Venezuela and the deserts of central Asia. These massive deposits will eventually be mined in the production of baking soda.</p>
					</div>
					<div class="item mb3">
						<h3>2500BC</h3>
						<p>Natron (a natural compound containing 17% sodium bicarbonate) is used by ancient Egyptians to dry and preserve meat, treat wounds, bleach clothing, soften water, remove oil and grease, clean teeth and freshen breath.</p>
					</div>

				</div>
			</div>

			<div class="block b2">
				<div class="vert"><img src="/images/heritage/side2.svg" alt="1840s" title="1840s"/></div>
				<div class="img"><img src="/images/heritage/blk2-image.jpg"/></div>
				<div class="items">
					<div class="item mb3">
						<h3>1846</h3>
						<p>John Dwight and his brother-in-law, Dr. Austin Church develop Bicarbonate of Soda (Baking Soda) as a leavening agent for home-baked goods. John Dwight &amp; Company is formed to manufacture and distribute the product.</p>
					</div>
				</div>
			</div>

			<div class="block b3">
				<div class="vert"><img src="/images/heritage/side3.svg" alt="1860s" title="1860s"/></div>
				<div class="img"><img src="/images/heritage/blk3-image.jpg"/></div>
				<div class="items">
					<div class="item">
						<h3>1865</h3>
						<p>Dr Church leaves John Dwight &amp; Company to open his own Baking Soda company with his two sons, James Austin Church and Elihu Dwight Church.</p>
					</div>
					<div class="item mb3">
						<h3>1867</h3>
						<p>Church &amp; Company is formed to meet the growing demand for Baking Soda. The ARM &amp; HAMMER&trade; trademark, symbolizing the strength of baking soda, shows the arm of Vulcan, the roman god of fire, bringing down his hammer on an anvil.</p>
					</div>

				</div>
			</div>


			<div class="block b4">
				<div class="vert"><img src="/images/heritage/side4.svg" alt="1870s" title="1870s"/></div>
				<div class="img"><img src="/images/heritage/blk4-image.jpg"/></div>
				<div class="items">
					<div class="item">
						<h3>1874</h3>
						<p>Church &amp; Co. introduces sodium carbonate-based ARM &amp; HAMMER&trade; Washing Soda, also known as Sal Soda, for heavier-duty cleaning chores.</p>
					</div>
					<div class="item mb3">
						<h3>1876</h3>
						<p>John Dwight, looking for a memorable trademark for his packaging chooses Lady Maud, a prize winning Jersey Cow. The cow is a reference to traditional use of Baking Soda and sour milk in home baking. Consumers eventually begin to call it COW BRAND.</p>
					</div>

				</div>
			</div>

			<div class="block b5">
				<div class="vert"><img src="/images/heritage/side5.svg" alt="1880s" title="1880s"/></div>
				<div class="img"><img src="/images/heritage/blk5-image.jpg"/></div>
				<div class="items">
					<div class="item mb3">
						<h3>1888</h3>
						<p>Product trading cards are a nationwide fad, and the first ARM &amp; HAMMER&trade; series entitled &ldquo;Beautiful Birds of America&rsquo;, appears. In the next five decades, over 500 artworks on sets of 15, 30 or 60 cards are published.</p>
					</div>

				</div>
			</div>

			<div class="block b6">
				<div class="vert"><img src="/images/heritage/side6.svg" alt="1890s" title="1890s"/></div>
				<div class="img"><img src="/images/heritage/blk6-image.jpg"/></div>
				<div class="items">
					<div class="item mb3">
						<h3>1896</h3>
						<p>After almost 30 years of friendly competition, the descendants of the founders of John Dwight &amp; Company and Church &amp; Co. join forces to form Church &amp; Dwight Company Inc.</p>
					</div>

				</div>
			</div>

			<div class="block b7">
				<div class="vert"><img src="/images/heritage/side7.svg" alt="1900s" title="1900s"/></div>
				<div class="img"><img src="/images/heritage/blk7-image.jpg"/></div>
				<div class="items">
					<div class="item">
						<h3>1907</h3>
						<p>Almost a century ahead of it&rsquo;s time, the company institutes the use of recycled paperboard to package household products.</p>
					</div>
					<div class="item mb3">
						<h3>1927</h3>
						<p>The first full-page colour magazine ads promote the purity of ARM &amp; HAMMER&trade; and COW BRAND Baking Soda for multiple kitchen and personal care issues.</p>
					</div>

				</div>
			</div>

			<div class="block b8">
				<div class="vert"><img src="/images/heritage/side8.svg" alt="1960s" title="1960s"/></div>
				<div class="img"><img src="/images/heritage/blk8-image.jpg"/></div>
				<div class="items">
					<div class="item">
						<h3>1960</h3>
						<p>New uses for Baking Soda were developed and tailored to changing post-war lifestyles. The multi-use Baking Soda wheel was created to familiarize consumers with the product&rsquo;s versatility.</p>
					</div>
					<div class="item mb3">
						<h3>1965</h3>
						<p>After marketing the same product under two brand names for over 60 years, the company consolidates ARM &amp; HAMMER&trade; and COW BRAND under the ARM &amp; HAMMER&trade; name.</p>
					</div>

				</div>
			</div>

			<div class="block b9">
				<div class="vert"><img src="/images/heritage/side9.svg" alt="1970s" title="1970s"/></div>
				<div class="img"></div>
				<div class="items">
					<div class="item mb5">
						<h3>1970</h3>
						<p>Church &amp; Dwight is the sole corporate sponsor of the first Earth day in April 1970.</p>
					</div>

				</div>
			</div>

			<div class="block b10">
				<div class="vert"><img src="/images/heritage/side10.svg" alt="1980s" title="1980s"/></div>
				<div class="img"><img src="/images/heritage/blk10-image.jpg"/></div>
				<div class="items">
					<div class="item mb3">
						<h3>1986</h3>
						<p>For it&rsquo;s 100th anniversary on July 4th, the Statue of Liberty&rsquo;s inner copper walls are cleaned with sodium bicarbonate, which removes 99 years of coal tar without damage to the copper. More than 100 tons of sodium bicarbonate are used in the restoration.</p>
					</div>

				</div>
			</div>

			<div class="block b11 last">
				<div class="vert"><img src="/images/heritage/side11.svg" alt="Today" title="Today"/></div>
				<div class="img"><img src="/images/heritage/blk11-image.jpg" alt="Arm &amp; Hammer Logo"/></div>
				<div class="ball"></div>
				<div class="items">
					<div class="item mb3">
						<p class="mt3">Baking Soda is now found in nearly every kitchen in the USA, regarded as a necessity by millions of consumers.</p>
						<p>Of course, Baking Soda is no longer confined to the kitchen. It is used throughout the house, in every phase of housekeeping and personal hygiene. ARM &amp; HAMMER&trade; Baking Soda, which was first prepared in a New England village, now is used the world over.</p>
					</div>

				</div>
			</div>

		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
