@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 intro" >
			<h1>Arm &amp; Hammer &lsquo;Exceptional Challenges&rsquo; prize draw 2016 - Terms and Conditions</h1>
		</div>
		<div class="columns span-12 mb2 panel" >
			<p><strong>Online Terms and Conditions </strong></p>
			<p>These Terms and Conditions prevail in the event of any conflict or inconsistency with any other communications, including advertising or promotional materials. Entry/claim instructions are deemed to form part of the terms and conditions and by participating all claimants will be deemed to have accepted and be bound by the terms and conditions. Please retain a copy for your information.</p>
			<ol>
				<li><strong>Promoter: </strong>Church &amp; Dwight Consumer Relations, Wear Bay Road, Folkestone, Kent, CT19 6PG.</li>
				<li><strong>Eligibility: </strong>Draw open to UK residents aged 18 years or over, excluding employees of Church &amp; Dwight UK Ltd., their families, agents and anyone else connected with this promotion.</li>
				<li><strong>Promotional Period: </strong>Only fully completed entries made between 00:00 on 01<sup>st</sup> January 2017 and 23:59 on 28<sup>th</sup> February 2017 will be accepted. No purchase necessary.</li>
				<li><strong>Entry Instructions: </strong>Only one entry per person. No entries from consumer groups. Entrants who do not give correct details or those entering on behalf of someone else will be disqualified, at the Promoter&rsquo;s discretion.</li>
				<li>Entry to the draw made by completing a review at <a href="http://www.boots.com/en/Arm-Hammer-Advance-White-Extreme-Whitening-Baking-Soda-Toothpaste-75ml_867625/" target="_blank">http://www.boots.com/en/Arm-Hammer-Advance-White-Extreme-Whitening-Baking-Soda-Toothpaste-75ml_867625/</a> , then submitting a screenshot of the review to armandhammer@live.co.uk. Entrants must submit a screenshot of their review, along with their Boots.com username and telephone number.</li>
				<li>The Promoter accepts no responsibility for entries/claims which are delayed, corrupted or otherwise rendered invalid owing to technical errors or malfunctions. Proof of sending is not proof of receipt.</li>
				<li><strong>Prize: </strong>One prize will be awarded, consisting of a bank transfer of &pound;100.</li>
				<li><strong>Winner Selection and Notification: </strong>The winner will be selected at random by an independent person or via a verifiable computer system producing random results and notified no later than 14 days after the promotion ends, by 14<sup>th</sup> March 2017 by email.</li>
				<li><strong>Winner Claim: </strong>The winner must confirm acceptance of the prize within 10 working days of being notified. The winner must provide all verification details as necessary including bank account number and sort code. The Promoter reserves the right to select a reserve winner in the event of a non-claim with no liability.</li>
				<li>Once claimed the prize will be delivered within 30 days of the verification process.</li>
				<li><strong>General: </strong>The prize winner&rsquo;s details (name and county) will be available for six weeks by sending an SAE marked &quot;Arm &amp; Hammer Exceptional Challenges prize draw&quot; to the Promoter at the address below after 14<sup>th</sup> March 2017.</li>
				<li>The prize winner consents to their name being published as the winner of the promotion, and may be required to participate in future publicity.</li>
				<li>The prize is as stated, and non-transferable.</li>
				<li>The decision of the Promoter is final and no correspondence will be entered into regarding the selection of winners.</li>
				<li>Access to this draw may be suspended temporarily and without notice in the case of system failure, maintenance or repair or for reasons beyond our control.</li>
				<li>The Promoter shall assume no responsibility or liability whatsoever for:</li>
				<ol style="list-style-type:lower-alpha;">
					<li>incorrect, incomplete or inaccurate entry information whether caused by an entrant, by any equipment or programming associated or utilised in this prize draw, or by any human error, which may occur in the processing of the entries; or</li>
					<li>any technical malfunctions of any telephone network or lines, computer online systems, networks, servers or providers, computer equipment, software, incomplete or garbled transmissions, or failure of email or media for any reason; or</li>
					<li>any injury or damage to any computer, computer accessory, software, or any other technology used by the entrant to enter the prize draw.</li>
				</ol>
				<li>Entrants agree to release Church &amp; Dwight UK Ltd, the Promoter and their parent, subsidiaries, affiliates, divisions, agents and employees, to the fullest extent permitted by law, from any and all liability, claims for damages arising out of the entrant&rsquo;s participation in the prize draw, the use of any entries as stated herein and the acceptance and use of any prize(s).</li>
				<li>Neither Church &amp; Dwight UK Ltd., nor any of their affiliates or agents accept any responsibility for any loss of the prize.</li>
				<li>By entering the prize draw, entrants agree to Church &amp; Dwight UK Ltd&rsquo;s privacy policy <a href="http://www.churchdwight.co.uk/privacy-policy" target="_blank">www.churchdwight.co.uk/privacy-policy</a>. Use of entrant details will be in accordance with its terms.</li>
				<li>Any attempt to defraud, abuse or violate these official rules in any way in connection with this prize draw may be prosecuted to the fullest extent of the law at the Promoter&#39;s sole and absolute discretion.</li>
				<li>Any question concerning the legal interpretation of the rules will be based on English law and the Courts of England and Wales will have exclusive jurisdiction.</li>
				<li>The Promotion is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to the Promoter, not Facebook. The information you provide will only be used for the purpose of facilitating the Promotion unless you have agreed to its use for any other purpose. By entering the Promotion, all Participants agree to give Facebook a complete release from any and all legal liability in connection with the Promotion. All entries will be subject to Facebook&rsquo;s terms of use which can be found at <a href="http://www.facebook.com/" target="_blank">www.facebook.com</a></li>
				<li>The Promoter reserves the right to withdraw or amend this promotion in the event of any unforeseen circumstances outside its reasonable control. This promotion and these terms are governed by English law and are subject to the exclusive jurisdiction of the English and Welsh Courts.</li>
				<li>Entrants are deemed to have accepted these terms and conditions by participating in this promotion. The terms and conditions of the Promoter&rsquo;s agents and third party contractors involved in the provision of the prize apply. The Promoter shall bear no liability in relation to services provided by these third parties.</li>
				<li>Where you have indicated that you would like us to contact you in the future, you agree that your relevant personal details will be held by Church &amp; Dwight UK Ltd. (&lsquo;C&amp;D&rsquo;) and may be used by C&amp;D, other companies within the C&amp;D group (details of which are available on request) or C&amp;D&rsquo;s agents, partners or licensees, to promote, to consider ways of improving, and to send you information about Arm &amp; Hammer&rsquo;s products and services. If in the future, you do not wish to receive further communications from the C&amp;D group and would prefer your details to be removed from its database, or if you simply wish to make corrections to your details or to how you receive communications from us, please inform us in writing at: Church &amp; Dwight Consumer Relations, Wear Bay Road, Folkestone, Kent, CT19 6PG.</li>
				<li>In order to request a copy of these terms and conditions by email or to make a complaint in connection with the promotion please contact the Promoter at the address below.</li>
				<li>If a review is submitted you give your permission for Church &amp; Dwight UK Ltd., its affiliates, agents and the Promoter to reuse or repurpose the review in future marketing material.</li>
			</ol>


		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
