@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 intro" >
			<!-- <h1>ORAL HEALTH</h1> -->
		</div>

		<div class="columns halfer" >
			<div class="block eq sensitive" group="grp1">
				<div class="eq-inner">
					<h1><img src="/images/oral-health/sensitive-teeth.png"/></h1>
					<div class="inner">
						<p>When you have sensitive teeth it can be painful, especially when munching on cold food or drink. The pain can be anything from a mild twinge to a strong, sharp discomfort.</p>
						<p>To ease the discomfort, try applying a pea sized amount of sensitive toothpaste to your toothbrush and evenly brush all the surfaces of your teeth.</p>
						<p><a href="/products/sensitive-type" class="btn">Sensitivity solutions</a></p>
					</div>
				</div>
				<img src="/images/shadow-left.png" class="shadow left"/>
				<img src="/images/shadow-left.png" class="shadow right"/>
			</div>
		</div>

		<div class="columns halfer" >
			<div class="block eq whitening" group="grp1">
				<div class="eq-inner">
					<h1><img src="/images/oral-health/whitening.png"/></h1>
					<div class="inner">
						<p>Your gnashers naturally darken with age and suffer from staining caused by some foods and drinks such as coffee, curry and red wine.</p>
						<p>BUT, fear not for there is a solution to make sure your teeth gleam and sparkle with whiteness. Click here to check out our super white enhancing toothpastes.</p>
						<p><a href="/products/white-answer" class="btn">Whitening solutions</a></p>
					</div>
				</div>
				<img src="/images/shadow-left.png" class="shadow left"/>
				<img src="/images/shadow-left.png" class="shadow right"/>
			</div>
		</div>
	</div>

	<div class="row full padded" style="margin-bottom: 3rem !important;">
		<div class="columns" >
			<div class="block enamel" group="grp1">
				<div class="eq-inner">
					<h1><img src="/images/oral-health/enamel-care.png" /></h1>
					<div class="inner">
						<p>Enamel is the hard stuff that protects your teeth. Over time this can get worn down which can lead to further problems. Dentin is what forms the bulk of your teeth and sits beneath the enamel. Dentin is naturally yellow in colour which can begin to show through as your enamel surface wears away. Fortunately, our range of exceptional enamel care toothpastes help to restore and strenghen enamel surface for better protection.</p>
						<p><a href="/products/enamel-pro" class="btn">Enamel Care solutions</a></p>
					</div>
				</div>
				<img src="/images/shadow-left.png" class="shadow left"/>
				<img src="/images/shadow-left.png" class="shadow right"/>
			</div>
		</div>

	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
