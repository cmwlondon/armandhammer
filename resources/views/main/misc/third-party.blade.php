@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 panel legal" style="min-height:450px;">
			<img src="/images/shadow.png" class="shadow">
			<h1>Third Party Information Collection</h1>
			<p>NONE</p>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
