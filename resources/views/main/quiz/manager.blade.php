<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <style>
      /*
       * Globals
       */

      /* Links */
      a,
      a:focus,
      a:hover {
        color: #fff;
      }

      /* Custom default button */
      .btn-secondary,
      .btn-secondary:hover,
      .btn-secondary:focus {
        color: #333;
        text-shadow: none; /* Prevent inheritance from `body` */
        background-color: #fff;
        border: .05rem solid #fff;
      }


      /*
       * Base structure
       */

      html,
      body {
        height: 100%;
        background-color: #333;
      }
      body {
        color: #fff;
        text-align: center;
        text-shadow: 0 .05rem .1rem rgba(0,0,0,.5);
      }

      /* Extra markup and styles for table-esque vertical and horizontal centering */
      .site-wrapper {
        display: table;
        width: 100%;
        height: 100%; /* For at least Firefox */
        min-height: 100%;
        -webkit-box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
                box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
      }
      .site-wrapper-inner {
        display: table-cell;
        vertical-align: top;
      }
      .cover-container {
        margin-right: auto;
        margin-left: auto;
      }

      /* Padding for spacing */
      .inner {
        padding: 2rem;
      }


      /*
       * Header
       */

      .masthead {
        margin-bottom: 2rem;
      }

      .masthead-brand {
        width:100%;
        text-align: center;
        margin-bottom: 0;
      }

      .nav-masthead .nav-link {
        padding: .25rem 0;
        font-weight: bold;
        color: rgba(255,255,255,.5);
        background-color: transparent;
        border-bottom: .25rem solid transparent;
      }

      .nav-masthead .nav-link:hover,
      .nav-masthead .nav-link:focus {
        border-bottom-color: rgba(255,255,255,.25);
      }

      .nav-masthead .nav-link + .nav-link {
        margin-left: 1rem;
      }

      .nav-masthead .active {
        color: #fff;
        border-bottom-color: #fff;
      }

      @media (min-width: 48em) {
        .masthead-brand {
          float: left;
        }
        .nav-masthead {
          float: right;
        }
      }


      /*
       * Cover
       */

      .cover {
        padding: 0 1.5rem;
      }
      .cover .btn-lg {
        padding: .75rem 1.25rem;
        font-weight: bold;
      }

      #managerTabs {
        padding-top:1rem;
      }

      /*
       * Affix and center
       */

      @media (min-width: 40em) {
        /* Pull out the header and footer */
        .masthead {
          position: fixed;
          top: 0;
        }
        .mastfoot {
          position: fixed;
          bottom: 0;
        }
        /* Start the vertical centering */
        .site-wrapper-inner {
          vertical-align: middle;
        }
        /* Handle the widths */
        .masthead,
        .mastfoot,
        .cover-container {
          width: 100%; /* Must be percentage or pixels for horizontal alignment */
        }
      }

      @media (min-width: 62em) {
        .masthead,
        .mastfoot,
        .cover-container {
          width: 42rem;
        }
      }
  </style>
  </head>
  <body>
     

     <div class="site-wrapper">

      <div class="site-wrapper-inner">
            
        <div class="cover-container">
          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Quiz Manager</h3>
            </div>
          </div>



          <div class="inner cover">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="stats-tab" data-toggle="tab" href="#stats" role="tab" aria-controls="stats" aria-expanded="true">Stats</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="threshold-tab" data-toggle="tab" href="#threshold" role="tab" aria-controls="threshold" aria-expanded="true">Threshold</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="download-tab" data-toggle="tab" href="#download" role="tab" aria-controls="download" aria-expanded="false">Download</a>
              </li>
            </ul>
            <div class="tab-content" id="managerTabs">
              <div class="tab-pane fade show active" id="stats" role="tabpanel" aria-labelledby="stats-tab">
                @if (isset($stats))
                  <p>Threshold: {{{$stats['threshold']}}}</p>
                  <p>Entries: {{{$stats['entries']}}}</p>
                @else
                {!! Form::open(array('url' => ['quiz/manager/stats'], 'method' => 'POST', 'id' => 'managerForm') )!!}
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                  </div>
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Show</button>
                  </div>
                {!! Form::close() !!}
                @endif
              </div>
              <div class="tab-pane fade show" id="threshold" role="tabpanel" aria-labelledby="threshold-tab">
                {!! Form::open(array('url' => ['quiz/manager/threshold'], 'method' => 'POST', 'id' => 'managerForm') )!!}
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                  </div>
                  <div class="form-group">
                    <label for="threshold">New Threshold</label>
                    <input type="number" class="form-control" id="threshold" placeholder="Number" name="threshold">
                  </div>
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Change</button>
                  </div>
                {!! Form::close() !!}
              </div>
              <div class="tab-pane fade show" id="download" role="tabpanel" aria-labelledby="download-tab">
                {!! Form::open(array('url' => ['quiz/manager/export'], 'method' => 'POST', 'id' => 'managerForm') )!!}
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                  </div>

                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="entriesrange" id="entriesrange1" value="today" checked>
                      Today's Entries
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="entriesrange" id="entriesrange2" value="all">
                      All Entries
                    </label>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Request</button>
                  </div>
                {!! Form::close() !!}
              </div>
              @if (session('status'))
                <div class="alert alert-{{ session('status-type') }}">
                    {{ session('status') }}
                </div>
            @endif
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>