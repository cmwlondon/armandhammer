@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div id="quizLockedContainer">
		<div id="animation_container" style="background-color:rgba(255, 255, 255, 0.00); width:750px; height:980px">
			<canvas id="canvas" width="750" height="980" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 0.00);"></canvas>
			<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:750px; height:980px; position: absolute; left: 0px; top: 0px; display: block;">
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
