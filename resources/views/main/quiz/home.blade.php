@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<img src="/images/quiz/ripple.svg" class="loading"/>

	<div id="quizContainer">
		<div id="animation_container" style="width:750px; height:980px">
			<canvas id="canvas" width="750" height="980" style="position: absolute; display: block;"></canvas>
			<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:750px; height:980px; position: absolute; left: 0px; top: 0px; display: block;">
			</div>
		</div>
	</div>

	<div class="hidden">
		<div id="form-content" class="form-content">
			<img src="/images/quiz/post-me-my-paste.png" class="w100"/>
			<div class="form">
				<div class="form-row">
					<label>Name:</label>
					<span><input type="text" value="" id="frmName" name="name"/></span>
				</div>
				<div class="form-row large">
					<label>Address:</label>
					<span><textarea id="frmAddress" name="address"></textarea></span>
				</div>
				<div class="form-row">
					<label>Email Address:</label>
					<span><input type="text" value="" id="frmEmail" name="email"/></span>
				</div>
				<div class="form-row optout">
					<label class="small">Opt out of future marketing:</label>
					<div class="checkbox optoutMarketing" id="optoutMarketing" data-selectable="true"></div>
					<img src="/images/quiz/post-it.png" class="btn btnSubmit" id="btnSubmit" data-selectable="true"/>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
