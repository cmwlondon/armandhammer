@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row full padded">
		<div class="columns span-12 intro" >
			@include('main.layouts.partials._shadow')
			<div class="row full">
				<div class="columns span-2 md-12 sm-12 tGrp eq" group="grp1">
					<img src="/images/science/tooth.png" class="tooth eq-inner"/>
				</div>

				<div class="columns span-10 md-12 sm-12 eq" group="grp1">
					<div class="inner eq-inner">
						<h1>Our Science</h1>

						<p>Do you want more from your toothpaste?<br>Arm &amp; Hammer daily toothpastes are the solution!</p>
						<p>We pride ourselves on our breakthrough dental technologies. Baking Soda is our hero ingredient and a completely natural cleaning agent that is gentle on tooth enamel. In combination with our toothpastes, clinically proven benefits can be achieved with effective plaque and stain removal in hard-to-reach areas of the mouth.</p>

						<a href="science#baking-soda" class="btn bScienceRouted">Read about Baking Soda</a>
						<a href="science#active-calcium" class="btn bScienceRouted">Read about Active Calcium</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div class="baking-soda">
		<div class="bg">
			<div class="row">
				<div class="col">
					<a name="#baking-soda"></a>
					<h1><img src="/images/science/tooth-sparkle.svg" class="icon"/><span>Baking Soda</span></h1>
				</div>
			</div>
			<div class="row">
				<div class="col helfwidth mixed">
					<div class="pic flexcolumn">
						<h3>OUR KICK-ASS NATURAL INGREDIENT</h3>
						<p class="mt1">At the very core of our unique Exceptional Experience is our miracle ingredient - Baking Soda. Not only is it effective at removing plaque, it also balances the pH in your mouth, whitens your teeth by breaking down stains, neutralises enamel damaging acids, cleans deeper in hard to reach places, is gentle on enamel&hellip; and freshens breath. You get the idea, it&rsquo;s really rather exceptional.</p>
					</div>
					<div class="figureBox">
						<figure class="soda"><img src="/images/science/baking-soda.jpg" alt="Baking Soda"/></figure>
					</div>
				</div>
				<div class="col halfwidth textonly">
					<h3>WHO KNEW?</h3>
					<ul>
						<li><strong>Baking Soda is clinically recognised</strong> as an incredibly powerful cleaning agent.  For example Arm &amp; Hammer Advance White<sup>TM</sup> Toothpaste removes up to 88% more plaque in hard to reach places than a leading toothpaste. How&rsquo;s that for&nbsp;exceptional?</li>
						<li><strong>Isn&rsquo;t Baking Soda abrasive? AS IF!</strong> Baking Soda is an incredibly gentle natural cleaner (despite packing a mighty punch) because it is softer than enamel, giving you all of the whitening power of a regular toothpaste with half the&nbsp;harshness.</li>
						<li><strong>What do we want? FRESHER BREATH.</strong> When do we want it? NOW. Good news, Baking Soda neutralises the pH in your mouth, blitzing all those horrible odours caused by bacteria. That&rsquo;s long-lasting freshness that you can really&nbsp;feel.</li>
						<li><strong>Trick or treat? No need for tricks here!</strong> Our unique Baking Soda formulation whitens by breaking down stains, revealing the natural whiteness of your gleaming gnashers. It&rsquo;s all treats&nbsp;here.</li>
					</ul>

				</div>
			</div>
			<div class="row">
				<div class="col tac">
					<a href="science#active-calcium" class="btn bScienceRouted">Read about Active Calcium</a>
				</div>
			</div>
		</div>
	</div>

	{{-- grp1 carousel --}}

	<div class="row full mt3 mb3">
		<div class="columns span-12 info protect">
			<div class="row full padded">
				<div class="columns span-12">
					<a name="#active-calcium"></a>
					<h1><img src="/images/science/tooth-shield.svg" class="icon"/><span>Active Calcium&trade;</span></h1>
				</div>
				<div class="columns span-6 sm-12 md-12">
					<h3>TRULY GROUNDBREAKING</h3>
					<p>Your body creates both calcium and phosphate ions which are found in your saliva. These ions are your body&rsquo;s way of defending your teeth from erosion.</p>
					<p class="mt1">With this in mind, we have developed Active Calcium&trade; that contains calcium and phosphate ions to help compliment your mouth&#39;s natural defense to protect your teeth more efficiently.</p>
					<p mt1class="">With everyday wear and tear, micro-cracks and crevices may begin to develop on your teeth creating an uneven enamel surface and making them appear increasingly dull and discoloured.</p>
					<p class="mt1">By using Arm &amp; Hammer&rsquo;s Total Pro Clean + Repair&trade;, Truly Radiant&trade;, Sensitive Pro&trade; or Enamel Pro Repair Toothpaste, Active Calcium&trade; will be deposited directly on to your tooth surface, filling the tiny crevices that form on your enamel surface, and binding to it. As a result, it restores the enamel surface making teeth smoother and whiter.</p>

				</div>
				<div class="columns span-6 sm-12 Emd-12 video">
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAACnCAYAAABgpoUkAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAcBJREFUeNrs1DEBAAAIwzDAv+fhgZdEQo92kgL4aCQADBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBAwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEDlAAwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQwAABDBDAAAEMEMAAAQwQMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDBAAAMEMEAAAwQwQAADBDhZAQYAPCMES9fvwf8AAAAASUVORK5CYII=" class="shim"/>
					<div id="contentPlayer1" class="mediaPlayer">
						<div id="contentContainer1" class="Player"></div>
					</div>
				</div>
				<div class="columns span-12 mt3 tac">
					<a href="science#baking-soda" class="btn bScienceRouted">Read about Baking Soda</a>
				</div>
			</div>
		</div>
	</div>

	{{-- grp2 carousel --}}

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
