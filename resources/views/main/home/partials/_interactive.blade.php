<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAdCAYAAADsMO9vAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADZJREFUeNrsz0ENAAAIBCC1f+ezgz83aEAnqc+mnhMQEBAQEBAQEBAQEBAQEBAQEBAQOFgBBgDjQgM3SY7WPwAAAABJRU5ErkJggg==" class="shim"/>

<div class="gallery">
	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAdCAYAAADsMO9vAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADZJREFUeNrsz0ENAAAIBCC1f+ezgz83aEAnqc+mnhMQEBAQEBAQEBAQEBAQEBAQEBAQOFgBBgDjQgM3SY7WPwAAAABJRU5ErkJggg==" class="shim"/>

	<img src="/images/home/packs-background.png" class="layer" usemap="#PacksMap"/>

	<div class="instruction">
		<p>GIVE US A SQUEEZE<br/><span>FOR A FLAVOUR OF THE ARM &amp; HAMMER EXPERIENCE</span></p>
	</div>


	<img src="/images/home/layer-mouthtastic.png" class="layer overlay hidden" data-ref="mouthtastic"/>
	<img src="/images/home/layer-tinglicious.png" class="layer overlay hidden" data-ref="tinglicious"/>
	

	<map name="PacksMap">
	  <area shape="poly" data-ref="mouthtastic" coords="553,1013,224,528,221,510,243,462,208,412,209,398,258,362,271,362,310,403,385,391,731,810,768,1012" href="#">
	  <area shape="poly" data-ref="mouthtastic" coords="769,1014,635,388,676,342,658,291,662,277,724,256,735,263,755,315,766,321,825,334,966,764,873,1013" href="#">
	  <area shape="poly" data-ref="tinglicious" coords="874,1014,1108,334,1123,326,1168,316,1184,257,1198,251,1256,263,1264,278,1256,331,1267,345,1300,373,1300,401,1174,1015" href="#">
	  <area shape="poly" data-ref="tinglicious" coords="1175,1014,1225,767,1548,407,1568,406,1608,417,1625,413,1657,371,1675,369,1722,406,1723,423,1691,465,1691,481,1706,519,1702,540,1376,1014" href="#">
	</map>
</div>