@extends('main.layouts.main')


@section('header')
	@include('main.home.partials._video')
	{{-- @include('main.home.partials._interactive') --}}
@endsection


@section('content')
	<div class="row">
		<div class="columns span-12 tac mt1">
			<h1 class="blkStyle">MEET THE VEGAN FAMILY</h1>
		</div>

		<div class="columns span-12 carousel-outer">
			<?php
			/*
			$products = ['advance-cavity-care',
							'advance-sensitive-care',
							'advance-white',
							'enamel-pro-repair',
							'sensitive-pro-repair',
							'total-pro',
							'truly-radiant'];
			*/
			// controls order of items in carousel, items in array used to reference image to display /public/images/products/pack-{item}.png
			// 'whitening-pro-protect' -> /public/images/products/pack-whitening-pro-protect.png
			$products = [
				'whitening-pro-protect',
				'advance-white',
				'sensitive-pro-repair',
				'natural-clean+white',
				'charcoal-white',
				'enamel-pro-repair'
			];

			?>

			@include('main.layouts.partials._carousel', ['group' => '1', 'products' => $products])
		</div>
	</div>

	<div class="row mb2">
		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAMAAACeL25MAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAApJREFUeAFjwAIAABQAAWG9rL8AAAAASUVORK5CYII=" class="svg-shim">
			<a href="https://www.facebook.com/armandhammeruk" target="_blank" class="svg-link">
				@include('main.home.partials._svg-join')
			</a>
		</div>
		<div class="columns span-4 md-6 sm-12">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAMAAACeL25MAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAApJREFUeAFjwAIAABQAAWG9rL8AAAAASUVORK5CYII=" class="svg-shim">
			<a href="/our-heritage" class="svg-link">
				@include('main.home.partials._svg-brand')
			</a>
		</div>
		<div class="columns span-4 md-6 md-before-3 sm-12">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAMAAACeL25MAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAApJREFUeAFjwAIAABQAAWG9rL8AAAAASUVORK5CYII=" class="svg-shim">
			<a href="/science" class="svg-link">
				@include('main.home.partials._svg-science')
			</a>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
