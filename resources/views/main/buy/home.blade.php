@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
	<div class="row">
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://www.boots.com/sitesearch?searchTerm=arm%20%26%20hammer" target="_blank" title="Boots"><img alt="Boots" src="/images/where-to-buy/boots.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://www.superdrug.com/search?text=arm+%26+Hammer" target="_blank" title="Superdrug"><img alt="Superdrug" src="/images/where-to-buy/superdrug.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://groceries.asda.com/search/Arm%20%26%20Hammer?cmpid=ahc-_-ghs-_-asdacom-_-hp-_-search-arm-%26-hammer" target="_blank" title="ASDA"><img alt="ASDA" src="/images/where-to-buy/asda.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://www.sainsburys.co.uk/gol-ui/SearchDisplayView?filters[keyword]=arm+%26+hammer&viewTaskName=SearchDisplayView&recipesSearch=true&orderBy=RELEVANCE&favouritesSelection=0&storeId=10151&langId=44&searchTerm=arm+%26+hammer&searchSubmit=Search" target="_blank" title="Sainsbury's"><img alt="Sainsbury's" src="/images/where-to-buy/sainsburys.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="http://www.tesco.com/groceries/product/search/default.aspx?searchBox=arm+%26+hammer&icid=tescohp_sws-1_arm+%26+hammer&N=4294795889" target="_blank" title="TESCO"><img alt="TESCO" src="/images/where-to-buy/tesco.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://groceries.morrisons.com/search?entry=arm%20%26%20hammer%20toothpaste" target="_blank" title="Morissons"><img alt="Morissons" src="/images/where-to-buy/morrisons.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://www.waitrose.com/ecom/shop/search?&searchTerm=arm%20%26%20hammer" target="_blank" title="Waitrose"><img alt="Waitrose" src="/images/where-to-buy/waitrose.jpg" class="logo"/></a>
		</div>
		<div class="columns mb3 span-4 md-6 sm-10 sm-before-1 sm-after-1">
			@include('main.layouts.partials._shadow')
			<a href="https://www.ocado.com/search?entry=arm%20%26%20hammer%20toothpaste" target="_blank" title="ocado"><img alt="ocado" src="/images/where-to-buy/ocado.png" class="logo"/></a>
		</div>
	</div>


@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
